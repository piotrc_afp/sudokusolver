import Control.Monad (sequence)
import Control.Monad.Trans.Except
import Control.Monad.IO.Class
import Control.Exception
import System.IO.Error
import Data.Set (fromList)
import Data.Char
import Text.Read

main = do 
  fileContent <- runExceptT getFileContent
  putStrLn "Please wait couple of seconds..."
  solvedSudoku <- runExceptT $ solveSudoku fileContent
  case solvedSudoku of
    Left error -> putStrLn error    
    Right sudoku -> putStr $ "\r\nSolution:\r\n" ++ formatSudoku sudoku

getFileContent :: ExceptT String IO String
getFileContent = do
  liftIO $ putStrLn "Please provide filename"
  fileName <- liftIO getLine
  eitherContent <- liftIO $ try $ readFile fileName
  case eitherContent of
    Left error -> throwE $ "File error: " ++ ioeGetErrorString (error :: IOError)
    Right content -> return content

solveSudoku :: Either String String -> ExceptT String IO [Int]
solveSudoku (Left error) = throwE error
solveSudoku (Right content) = 
  case (parseSudoku content) of
    Left error -> throwE error
    Right arr ->
      case (getSolution arr) of
        Left error -> throwE error
        Right sol -> return $ sol

formatSudoku :: [Int] -> String
formatSudoku [] = ""
formatSudoku su = formatRows rows ++ "\r\n" ++ formatSudoku rest
  where (rows, rest) = splitAt 27 su
        formatRows [] = ""
        formatRows rows = formatLine line ++ "\r\n" ++ formatRows tail1
          where (line, tail1) = splitAt 9 rows
                formatLine [] = ""
                formatLine line = formatCell cell ++ " " ++ formatLine tail2
                  where (cell, tail2) = splitAt 3 line
                        formatCell cell = concatMap (\x -> show x ++ " ") cell

getSolution :: [Int] -> Either String [Int]
getSolution sudoku = 
  if null results then 
    Left "Detected conflicts in sudoku -> cannot solve it" 
  else 
    Right $ results !! 0
  where emptyCells = [x | x <- [0..80], sudoku !! x == 0]
        results = allSolutions emptyCells sudoku

replaceWithRange :: Int -> [Int] -> [Int] -> [[Int]]
replaceWithRange _ [] source = [source]
replaceWithRange n values source = 
  let (start, end) = splitAt n source in
  map (\x -> start ++ x : (drop 1 end)) values

allSolutions :: [Int] -> [Int] -> [[Int]]
allSolutions [] su = if isValidSudoku su then [su] else []
allSolutions (x:xs) su = concatMap (allSolutions xs) $ filter (isValidSudoku) (replaceWithRange x [1..9] su)

validateValues :: Either String [Int] -> Either String [Int]
validateValues (Left _) =  Left "Non-integer values provided in file"
validateValues (Right values) =
  if all (\x -> (x >=0 && x <= 9)) values then
    Right values
  else
    Left "Sudoku can only have 1-9 values. Use 0 to indicate empty cell"

readEitherInt :: String -> Either String Int
readEitherInt str = readEither str

parseLine :: String -> Either String [Int]
parseLine line = 
  if length elements /= 9 then
    Left "There should be nine sudoku values in each line"
  else
    validateValues $ sequence $ map readEitherInt elements
  where elements = words line

parseSudoku :: String -> Either String [Int]
parseSudoku text = 
  if length fileLines /= 9 then
    Left "There should be nine sudoku lines in file"
  else
    concat <$> (sequence $ map parseLine fileLines)
  where fileLines = filter (not . all isSpace) $ lines $ trim text

trim str = dropWhile isSpace $ dropTail str

dropTail str = take (length str - nonSpacesFromEnd) str
    where nonSpacesFromEnd = length $ takeWhile isSpace (reverse str)

rowIndices i = map (+9*i) [0..8]
colIndices i = map (+i) [0,9..72]
squIndices i = map (+(3*i + 18*(i `div` 3))) $ base ++ (map (+9) base) ++ (map (+9*2) base)
                where base = [0,1,2]

row sudoku i = map (sudoku !!) $ rowIndices i
column sudoku i = map (sudoku !!) $ colIndices i
square sudoku i = map (sudoku !!) $ squIndices i

hasDuplicates list = length list /= length set
    where set = fromList list

validRow sudoku i = not $ hasDuplicates $ filter (>0) $ row sudoku i
validColumn sudoku i = not $ hasDuplicates $ filter (>0) $ column sudoku i
validSquare sudoku i = not $ hasDuplicates $ filter (>0) $ square sudoku i

isValidSudoku sudoku =  all (==True) (map (validColumn sudoku) [0..8]) && 
                        all (==True) (map (validSquare sudoku) [0..8]) &&
                        all (==True) (map (validRow sudoku) [0..8])